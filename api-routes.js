// Filename: api-routes.js
// Initialize express router
let router = require('express').Router();
var userController = require('./controllers/usersController');
// 1) Users Controller 
router.route('/users')
    .get(userController.index);
// router.get('/', function (req, res) {
//     res.json({
//         status: 'API Its Working',
//         message: 'Welcome to Api Manager crafted with love!'
//     });
// });
router.route('/contacts/:contact_id')
    .get(userController.view)
    .patch(userController.update)
    .put(userController.update)
    .delete(userController.delete);
// Export API routes
module.exports = router;