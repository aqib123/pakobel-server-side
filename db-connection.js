let mongoose = require('mongoose');
// Connect to Mongoose and set connection variable
// Deprecated: mongoose.connect('mongodb://localhost/resthub');
mongoose.connect('mongodb://localhost/db_maila', { useNewUrlParser: true, useUnifiedTopology: true});
var db = mongoose.connection;

module.exports = db;