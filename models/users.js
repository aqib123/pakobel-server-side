var mongoose = require('mongoose');
// Setup schema
var usersSchema = mongoose.Schema({
    name: {
        type: String,
        required: true
    },
    email: {
        type: String,
        required: true
    },
    gender: String,
    phone: String,
    create_date: {
        type: Date,
        default: Date.now
    }
});
// Export Contact model
var User = module.exports = mongoose.model('accounts', usersSchema);
module.exports.get = function (callback, limit) {
    User.find(callback).limit(limit);
}