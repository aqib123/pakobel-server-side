// FileName: index.js
// Import express
let express = require('express')
// Import Body parser
let bodyParser = require('body-parser');
// Import routes
let apiRoutes = require("./api-routes")
// Initialize the app
let app = express();
// Database Connection
let db = require("./db-connection");
// Added check for DB connection
if(!db)
    console.log("Error connecting db")
else
    console.log("Db connected successfully")
// Setup server port
var port = process.env.PORT || 8080;
// Use Api routes in the App
app.use('/api', apiRoutes)
// Configure bodyparser to handle post requests
app.use(bodyParser.urlencoded({
    extended: true
 }));
// Send message for default URL
app.get('/', (req, res) => res.send('Welcome to Pakobel API Manager Serevr.'))
// Launch app to listen to specified port
app.listen(port, function () {
     console.log("Running API Manager on port " + port)
});